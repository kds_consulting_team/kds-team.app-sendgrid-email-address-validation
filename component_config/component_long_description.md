This component enables users to validate emails from tables in keboola for there uses in email marketing or other
usecases. The validator returns estimates on validity and performed checks. 