import sendgrid as sg
from retry import retry
from python_http_client.exceptions import ForbiddenError, InternalServerError, ServiceUnavailableError
from python_http_client.exceptions import GatewayTimeoutError, TooManyRequestsError
from keboola.component.base import UserException

from email_validator import EmailValidator


class SendgridEmailValidator(EmailValidator):
    RESULT_KEY = "result"
    OUTPUT_COLUMNS = ["email", "verdict", "score", "local", "host", "checks_domain_has_valid_address_syntax",
                      "checks_domain_has_mx_or_a_record", "checks_domain_is_suspected_disposable_address",
                      "checks_local_part_is_suspected_role_address", "checks_additional_has_known_bounces",
                      "checks_additional_has_suspected_bounces", "ip_address"]
    INPUT_COLUMNS = ["email", "source"]

    def __init__(self):
        self.client = []
        self.email_column = ""
        self.source_column = ""
        self.output_columns = self.OUTPUT_COLUMNS
        self.api_key = []

    def authorize(self, auth_dict):
        api_key = auth_dict.get("api_key")
        self.client = self.get_client(api_key)

    @staticmethod
    def get_client(api_key):
        sg_client = sg.SendGridAPIClient(api_key)
        return sg_client

    def set_api_parameters(self, email_column="", source_column=""):
        self.email_column = email_column
        self.source_column = source_column
        self.not_fetched_output_columns = [email_column]
        if source_column:
            self.not_fetched_output_columns = [email_column, source_column]

    def validate_email(self, input_mail):
        request_params = self.get_request_params(input_mail)
        result = self.fetch_result(self.client, request_params)
        if result:
            result = self.flatten_result(result[self.RESULT_KEY])
            result = self.normalize_result(result)
            return result
        return False

    def fetch_result(self, client, request_params):
        try:
            result = self._get_result(client, request_params)
            return result
        except ForbiddenError as http_error:
            raise Exception("HTTP Error 403: Forbidden. API Key for email validation is invalid") from http_error
        except Exception:
            # Continue even if fetching result fails
            pass

    @retry((InternalServerError, ServiceUnavailableError, GatewayTimeoutError, TooManyRequestsError), tries=3, delay=1)
    def _get_result(self, sg_client, request_params):
        return sg_client.client.validations.email.post(request_body=request_params).to_dict

    def get_request_params(self, input_row):
        try:
            request_params = {"email": input_row[self.email_column]}
        except KeyError as key_error:
            raise UserException("Email column specified, but not found in input table") from key_error
        if self.source_column:
            try:
                request_params["source"] = input_row[self.source_column]
            except KeyError as key_error:
                raise UserException("Source column specified, but not found in input table") from key_error
        return request_params

    def normalize_result(self, result):
        new_result = {}
        for col in self.OUTPUT_COLUMNS:
            if col in result.keys():
                new_result[col] = result[col]
            else:
                new_result[col] = ""
        return new_result
