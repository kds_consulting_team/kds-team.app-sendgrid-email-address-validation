import abc


class EmailValidator(metaclass=abc.ABCMeta):

    @abc.abstractmethod
    def authorize(self, auth_dict):
        pass

    @abc.abstractmethod
    def set_api_parameters(self, input_columns):
        pass

    def flatten_result(self, json_dict, delim="_"):
        flattened_dict = {}
        for i in json_dict.keys():
            if isinstance(json_dict[i], dict):
                get = self.flatten_result(json_dict[i], delim)
                for j in get.keys():
                    new_key = "".join([i, delim, j])
                    flattened_dict[new_key] = get[j]
            else:
                flattened_dict[i] = json_dict[i]

        return flattened_dict
